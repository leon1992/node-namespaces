import { PartialReadOnlyDeep } from './PartialReadOnlyDeep';

export interface Store<T> {
  readonly [asyncId: number]: PartialReadOnlyDeep<T>;
}
