import * as asyncHooks from 'async_hooks';
import * as _ from 'lodash';

import { AsyncCallbacks } from './AsyncHooks';
import { PartialReadOnlyDeep } from './index';
import { NamespaceAlreadyExistsError } from './NamespaceAlreadyExistsError';
import { Store } from './Store';

export class Namespace<T extends object = any> {
  /**
   * Store references for every created namespace
   */
  private static namespaces: Map<string, Namespace> = new Map();

  /**
   * Namespace store store
   */
  readonly store: Store<T> = {};

  /**
   * Namespace name
   */
  readonly name: string;

  /**
   * Async hook callback
   */
  private callbacks = new AsyncCallbacks(this);
  private hooks = asyncHooks.createHook(this.callbacks);

  /**
   * Destroy every namespace created. Usefull for testing purposes
   */
  static clear() {
    for (const [key, val] of this.namespaces.entries()) {
      val.disable();
      this.namespaces.delete(key);
    }
  }

  constructor(name: string) {
    if (Namespace.namespaces.has(name)) {
      throw new NamespaceAlreadyExistsError(name);
    }

    this.name = name;
    Namespace.namespaces.set(this.name, this);
  }

  /**
   * Enable async hooks
   */
  enable(): this {
    this.hooks.enable();

    return this;
  }

  /**
   * Disable async hooks
   */
  disable(): this {
    this.hooks.disable();

    return this;
  }

  /**
   * Get an item from the store
   * @param key Store path. Used by `_.get`
   */
  get<K extends keyof T>(key: K): T[K] | undefined {
    const store = this.getCurrentStore();

    return _.get(store, key);
  }

  /**
   * Set an item to the store
   * @param key Store path. Used by `_.set`
   * @param value Store value
   */
  set<K extends keyof T>(key: K, value: T[K]): this {
    const store = this.getCurrentStore();

    _.set(store, key, value);

    return this;
  }

  /**
   * Creates a store with the current exceution async id
   */
  createStore(): this {
    const eid = asyncHooks.executionAsyncId();

    _.set(this.store, eid, {});

    return this;
  }

  /**
   * Get the store for the current execution async id
   */
  getCurrentStore(): PartialReadOnlyDeep<T> {
    const eid = asyncHooks.executionAsyncId();

    return this.store[eid];
  }

  restify() {
    const restifyAsyncStore = (req, res, next) => {
      this.createStore()
        .set('req' as any, req)
        .set('res' as any, res);

      next();
    };

    return restifyAsyncStore;
  }

  express() {
    const expressAsyncStore = (req, res, next) => {
      this.createStore()
        .set('req' as any, req)
        .set('res' as any, res);

      next();
    };

    return expressAsyncStore;
  }

  hapi() {
    const hapiAsyncStore = (request, reply) => {
      this.createStore()
        .set('request' as any, request)
        .set('reply' as any, reply);

      return reply.continue();
    };

    return hapiAsyncStore;
  }
}
