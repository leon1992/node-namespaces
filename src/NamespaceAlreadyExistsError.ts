export class NamespaceAlreadyExistsError extends Error {
  constructor(name: string) {
    super(`The namespace ${name} already exists`);
  }
}
