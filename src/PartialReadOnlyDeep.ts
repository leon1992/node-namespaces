export type PartialReadOnlyDeep<T> = { readonly [K in keyof T]?: PartialReadOnlyDeep<T[K]> };
