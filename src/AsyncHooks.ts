import * as _ from 'lodash';

import { Namespace } from './Namespace';

export class AsyncCallbacks {
  constructor(private namespace: Namespace) {}

  /**
   * Called when a class is constructed that has the possibility to emit an asynchronous event.
   * @param asyncId a unique ID for the async resource
   * @param type the type of the async resource
   * @param triggerAsyncId the unique ID of the async resource in whose execution context this async resource was created
   */
  init = (asyncId: number, type: string, triggerAsyncId: number): void => {
    if (this.namespace.store[triggerAsyncId]) {
      _.set(this.namespace.store, asyncId, this.namespace.store[triggerAsyncId]);
    }
  }

  /**
   * Called after the resource corresponding to asyncId is destroyed
   * @param asyncId a unique ID for the async resource
   */
  destroy = (asyncId: number): void => {
    delete (<any>this.namespace.store)[asyncId];
  }
}
