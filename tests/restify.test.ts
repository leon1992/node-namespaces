import { expect } from 'chai';
import * as restify from 'restify';
import * as supertest from 'supertest';

import { Namespace } from '../src';

let s: restify.Server;

describe('restify', function() {
  afterEach(clean);

  it('should set the request in the namespace store', async function() {
    const ns = new Namespace('test').enable();
    let req;

    await withServer(ns, function() {
      req = ns.get('req');
    });

    await supertest(s).get('/');
    expect(req).to.exist;
  });

  it('should set the response in the namespace store', async function() {
    const ns = new Namespace('test').enable();
    let res;

    await withServer(ns, function() {
      res = ns.get('res');
    });

    await supertest(s).get('/');
    expect(res).to.exist;
  });

  context('simultaneous requests', function() {
    it('the namespace request should be different in every request', async function() {
      const ns = new Namespace('test').enable();

      const requests = [];

      await withServer(ns, function() {
        requests.push(ns.get('req'));
      });

      await Promise.all([supertest(s).get('/'), supertest(s).get('/')]);
      expect(requests).to.have.lengthOf(2);
      expect(requests[0]).to.not.be.equals(requests[1]);
    });

    it('the namespace response should be different in every request', async function() {
      const ns = new Namespace('test').enable();

      const responses = [];

      await withServer(ns, function() {
        responses.push(ns.get('res'));
      });

      await Promise.all([supertest(s).get('/'), supertest(s).get('/')]);
      expect(responses).to.have.lengthOf(2);
      expect(responses[0]).to.not.be.equals(responses[1]);
    });
  });
});

async function withServer(ns: Namespace, callback: () => void) {
  s = restify.createServer();

  s.use(ns.restify());

  s.get('/', (req, res, next) => {
    try {
      callback();
      res.send({ ok: true });
    } catch (err) {
      next(err);
    }
  });

  await new Promise((resolve, reject) => {
    s.listen(0, err => (err ? reject(err) : resolve()));
  });
}

async function clean() {
  if (s && s.server.listening) {
    await new Promise((resolve, reject) => {
      s.close(err => (err ? reject(err) : resolve()));
    });
  }

  Namespace.clear();
}
