import { expect } from 'chai';
import * as hapi from 'hapi';
import * as http from 'http';
import * as supertest from 'supertest';

import { Namespace } from '../src';

let hapiServer: hapi.Server;
let s: http.Server;

describe('Hapi', function() {
  afterEach(clean);

  it('should set the request in the namespace store', async function() {
    const ns = new Namespace('test').enable();
    let request;

    await withServer(ns, function() {
      request = ns.get('request');
    });

    await supertest(s).get('/');

    expect(request).to.exist;
  });

  it('should set the "reply" in the namespace store', async function() {
    const ns = new Namespace('test').enable();
    let h;

    await withServer(ns, function() {
      h = ns.get('reply');
    });

    await supertest(s).get('/');
    expect(h).to.exist;
  });

  context('simultaneous requests', function() {
    it('the namespace request should be different in every request', async function() {
      const ns = new Namespace('test').enable();

      const requests = [];

      await withServer(ns, function() {
        requests.push(ns.get('request'));
      });

      await Promise.all([supertest(s).get('/'), supertest(s).get('/')]);
      expect(requests).to.have.lengthOf(2);
      expect(requests[0]).to.not.be.equals(requests[1]);
    });

    it('the namespace "reply" should be different in every request', async function() {
      const ns = new Namespace('test').enable();

      const hs = [];

      await withServer(ns, function() {
        hs.push(ns.get('reply'));
      });

      await Promise.all([supertest(s).get('/'), supertest(s).get('/')]);
      expect(hs).to.have.lengthOf(2);
      expect(hs[0]).to.not.be.equals(hs[1]);
    });
  });
});

async function withServer(ns: Namespace, callback: () => void) {
  s = http.createServer();
  hapiServer = new hapi.Server();
  hapiServer.connection({ port: 0, listener: s });

  hapiServer.ext('onRequest', ns.hapi());

  hapiServer.route({
    method: 'get',
    path: '/',
    handler(request, reply) {
      callback();
      reply({ ok: true });
    }
  });

  await hapiServer.start();
}

async function clean() {
  await hapiServer.stop();

  Namespace.clear();
}
