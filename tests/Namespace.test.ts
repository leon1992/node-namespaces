import { expect } from 'chai';

import { Namespace, NamespaceAlreadyExistsError } from '../src';

describe('Namespace', function() {
  afterEach(() => Namespace.clear());

  describe('#constructor()', function() {
    it('should create a new namespace without failing', async function() {
      const ns = new Namespace('test namespace');
    });

    it('should fail to create a namespace with the same name', async function() {
      const ns = new Namespace('test');

      expect(() => new Namespace('test')).to.throw(NamespaceAlreadyExistsError);
    });
  });

  describe('#createStore()', function() {
    it('should create a store and fetch it in the following execution', async function() {
      const ns = new Namespace('test')
        .enable()
        .createStore()
        .set('test', 1);

      await promise().then(() => {
        expect(ns.get('test')).to.be.equals(1);
      });
    });
  });

  it('should created two different stores and its values should not collide', async function() {
    const ns = new Namespace('test').enable();

    const firstExecution = async () => {
      ns.createStore().set('test', 1);

      await promise().then(() => {
        expect(ns.get('test')).to.be.equals(1);
      });
    };

    const secondExecution = async () => {
      ns.createStore().set('test', 2);

      await promise().then(() => {
        expect(ns.get('test')).to.be.equals(2);
      });
    };

    await Promise.all([firstExecution(), secondExecution()]);
  });
});

async function promise() {}
